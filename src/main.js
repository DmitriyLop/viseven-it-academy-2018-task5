import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);
import gallery from './components/gallery.vue';
import cart from './components/cart.vue';
import category from './components/category.vue';

const routes = [
    { path: '/', component: gallery, name: 'gallery', props:true},
    {path: '/cart', component: cart, name: 'cart'},
    {path: '/category/:name', component: category},
];


const router = new VueRouter({
    routes // сокращённая запись для `routes: routes`
});


new Vue({
    router,
    render: h => h(App),
}).$mount('#app');
